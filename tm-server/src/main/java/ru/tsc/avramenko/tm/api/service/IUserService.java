package ru.tsc.avramenko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.IService;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.model.User;
import java.util.List;

public interface IUserService extends IService<User> {

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User removeByLogin(@Nullable String login);

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    User setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    User setRole(@Nullable String id, @Nullable Role role);

    boolean isLoginExist(@NotNull String login);

    boolean isEmailExist(@NotNull String email);

    @NotNull
    User updateUserById(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName, @Nullable String email);

    @NotNull
    User updateUserByLogin(@Nullable String login, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName, @Nullable String email);

    @NotNull
    User lockUserByLogin(@Nullable String login);

    @NotNull
    User unlockUserByLogin(@Nullable String login);

}