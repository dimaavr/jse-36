package ru.tsc.avramenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.exception.entity.TaskNotFoundException;
import ru.tsc.avramenko.tm.exception.system.ProcessException;
import ru.tsc.avramenko.tm.model.Task;

import java.util.List;

public class TaskRepositoryTest {

    @Nullable
    private TaskRepository taskRepository;

    @Nullable
    private Task task;

    @NotNull
    protected static final String TEST_TASK_NAME = "TestName";

    @NotNull
    protected static final String TEST_DESCRIPTION_NAME = "TestDescription";

    @NotNull
    protected static final String TEST_USER_ID = "TestUserId";

    @NotNull
    protected static final String TEST_USER_ID_INCORRECT = "TestUserIdIncorrect";

    @NotNull
    protected static final String TEST_TASK_ID_INCORRECT = "647";

    @Before
    public void before() {
        taskRepository = new TaskRepository();
        task = taskRepository.add(TEST_USER_ID, new Task(TEST_TASK_NAME, TEST_DESCRIPTION_NAME));
    }

    @Test
    public void add() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        Assert.assertNotNull(task.getName());
        Assert.assertNotNull(task.getDescription());
        Assert.assertEquals(TEST_TASK_NAME, task.getName());
        Assert.assertEquals(TEST_DESCRIPTION_NAME, task.getDescription());

        @NotNull final Task taskById = taskRepository.findById(task.getId());
        Assert.assertNotNull(taskById);
        Assert.assertEquals(task, taskById);
    }

    @Test
    public void findAll() {
        @NotNull final List<Task> tasks = taskRepository.findAll();
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void existsById() {
        Assert.assertTrue(taskRepository.existsById(TEST_USER_ID, task.getId()));
    }

    @Test
    public void findAllByUserId() {
        @NotNull final List<Task> tasks = taskRepository.findAll(TEST_USER_ID);
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void findAllByUserIdIncorrect() {
        @NotNull final List<Task> tasks = taskRepository.findAll(TEST_USER_ID_INCORRECT);
        Assert.assertNotEquals(1, tasks.size());
    }

    @Test
    public void findById() {
        @NotNull final Task task = taskRepository.findById(TEST_USER_ID, this.task.getId());
        Assert.assertNotNull(task);
    }

    @Test(expected = ProcessException.class)
    public void findByIdIncorrect() {
        @NotNull final Task task = taskRepository.findById(TEST_USER_ID, TEST_TASK_ID_INCORRECT);
        Assert.assertNull(task);
    }

    @Test(expected = ProcessException.class)
    public void findByIdNull() {
        @NotNull final Task task = taskRepository.findById(TEST_USER_ID, null);
        Assert.assertNull(task);
    }

    @Test(expected = ProcessException.class)
    public void findByIdIncorrectUser() {
        @NotNull final Task task = taskRepository.findById(TEST_USER_ID_INCORRECT, this.task.getId());
        Assert.assertNull(task);
    }

    @Test
    public void remove() {
        taskRepository.removeById(task.getId());
        Assert.assertNull(taskRepository.findById(task.getId()));
    }

    @Test
    public void findByName() {
        @NotNull final Task task = taskRepository.findByName(TEST_USER_ID, TEST_TASK_NAME);
        Assert.assertNotNull(task);
    }

    @Test(expected = TaskNotFoundException.class)
    public void findByNameIncorrect() {
        @NotNull final Task task = taskRepository.findByName(TEST_USER_ID, TEST_TASK_ID_INCORRECT);
        Assert.assertNull(task);
    }

    @Test(expected = TaskNotFoundException.class)
    public void findByNameNull() {
        @NotNull final Task task = taskRepository.findByName(TEST_USER_ID, null);
        Assert.assertNull(task);
    }

    @Test(expected = TaskNotFoundException.class)
    public void findByNameIncorrectUser() {
        @NotNull final Task task = taskRepository.findByName(TEST_USER_ID_INCORRECT, this.task.getName());
        Assert.assertNull(task);
    }

    @Test
    public void findByIndex() {
        @NotNull final Task task = taskRepository.findByIndex(TEST_USER_ID, 0);
        Assert.assertNotNull(task);
    }

    @Test
    public void removeById() {
        taskRepository.removeById(TEST_USER_ID, task.getId());
        Assert.assertNull(taskRepository.findById(task.getId()));
    }

    @Test(expected = ProcessException.class)
    public void removeByIdNull() {
        Assert.assertNull(taskRepository.removeById(TEST_USER_ID, null));
    }

    @Test(expected = ProcessException.class)
    public void removeByIdIncorrect() {
        @NotNull final Task task = taskRepository.removeById(TEST_USER_ID, TEST_TASK_ID_INCORRECT);
        Assert.assertNull(task);
    }

    @Test(expected = ProcessException.class)
    public void removeByIdIncorrectUser() {
        @NotNull final Task task = taskRepository.removeById(TEST_USER_ID_INCORRECT, this.task.getId());
        Assert.assertNull(task);
    }

    @Test
    public void removeByIndex() {
        @NotNull final Task task = taskRepository.removeByIndex(TEST_USER_ID, 0);
        Assert.assertNotNull(task);
    }

    @Test
    public void removeByName() {
        @NotNull final Task task = taskRepository.removeByName(TEST_USER_ID, TEST_TASK_NAME);
        Assert.assertNotNull(task);
    }

    @Test(expected = TaskNotFoundException.class)
    public void removeByNameIncorrect() {
        @NotNull final Task task = taskRepository.removeByName(TEST_USER_ID, TEST_TASK_ID_INCORRECT);
        Assert.assertNull(task);
    }

    @Test(expected = TaskNotFoundException.class)
    public void removeByNameNull() {
        @NotNull final Task task = taskRepository.removeByName(TEST_USER_ID, null);
        Assert.assertNull(task);
    }

    @Test(expected = TaskNotFoundException.class)
    public void removeByNameIncorrectUser() {
        @NotNull final Task task = taskRepository.removeByName(TEST_USER_ID_INCORRECT, this.task.getName());
        Assert.assertNull(task);
    }

    @Test
    public void startById() {
        @Nullable final Task task = taskRepository.startById(TEST_USER_ID, this.task.getId());
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test(expected = ProcessException.class)
    public void startByIdNull() {
        @Nullable final Task task = taskRepository.startById(TEST_USER_ID, null);
        Assert.assertNull(task);
    }

    @Test(expected = ProcessException.class)
    public void startByIdIncorrect() {
        @Nullable final Task task = taskRepository.startById(TEST_USER_ID, TEST_TASK_ID_INCORRECT);
        Assert.assertNull(task);
    }

    @Test(expected = ProcessException.class)
    public void startByIdIncorrectUser() {
        @Nullable final Task task = taskRepository.startById(TEST_USER_ID_INCORRECT, this.task.getId());
        Assert.assertNull(task);
    }

    @Test
    public void startByName() {
        @Nullable final Task task = taskRepository.startByName(TEST_USER_ID, TEST_TASK_NAME);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test(expected = TaskNotFoundException.class)
    public void startByNameNull() {
        @Nullable final Task task = taskRepository.startByName(TEST_USER_ID, null);
        Assert.assertNull(task);
    }

    @Test(expected = TaskNotFoundException.class)
    public void startByNameIncorrect() {
        @Nullable final Task task = taskRepository.startByName(TEST_USER_ID, TEST_TASK_ID_INCORRECT);
        Assert.assertNull(task);
    }

    @Test(expected = TaskNotFoundException.class)
    public void startByNameIncorrectUser() {
        @Nullable final Task task = taskRepository.startByName(TEST_USER_ID_INCORRECT, TEST_TASK_NAME);
        Assert.assertNull(task);
    }

    @Test
    public void startByIndex() {
        @Nullable final Task task = taskRepository.startByIndex(TEST_USER_ID, 0);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test
    public void startByIndexIncorrect() {
        @Nullable final Task task = taskRepository.startByIndex(TEST_USER_ID, 674);
        Assert.assertNull(task);
    }

    @Test
    public void startByIndexIncorrectUser() {
        @Nullable final Task task = taskRepository.startByIndex(TEST_USER_ID_INCORRECT, 674);
        Assert.assertNull(task);
    }

    @Test
    public void finishById() {
        @Nullable final Task task = taskRepository.finishById(TEST_USER_ID, this.task.getId());
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test(expected = ProcessException.class)
    public void finishByIdNull() {
        @Nullable final Task task = taskRepository.finishById(TEST_USER_ID, null);
        Assert.assertNull(task);
    }

    @Test(expected = ProcessException.class)
    public void finishByIdIncorrect() {
        @Nullable final Task task = taskRepository.finishById(TEST_USER_ID, TEST_TASK_ID_INCORRECT);
        Assert.assertNull(task);
    }

    @Test(expected = ProcessException.class)
    public void finishByIdIncorrectUser() {
        @Nullable final Task task = taskRepository.finishById(TEST_USER_ID_INCORRECT, this.task.getId());
        Assert.assertNull(task);
    }

    @Test
    public void finishByName() {
        @Nullable final Task task = taskRepository.finishByName(TEST_USER_ID, TEST_TASK_NAME);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test(expected = TaskNotFoundException.class)
    public void finishByNameNull() {
        @Nullable final Task task = taskRepository.finishByName(TEST_USER_ID, null);
        Assert.assertNull(task);
    }

    @Test(expected = TaskNotFoundException.class)
    public void finishByNameIncorrect() {
        @Nullable final Task task = taskRepository.finishByName(TEST_USER_ID, TEST_TASK_ID_INCORRECT);
        Assert.assertNull(task);
    }

    @Test(expected = TaskNotFoundException.class)
    public void finishByNameIncorrectUser() {
        @Nullable final Task task = taskRepository.finishByName(TEST_USER_ID_INCORRECT, TEST_TASK_NAME);
        Assert.assertNull(task);
    }

    @Test
    public void finishByIndex() {
        @Nullable final Task task = taskRepository.finishByIndex(TEST_USER_ID, 0);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test
    public void finishByIndexIncorrect() {
        @Nullable final Task task = taskRepository.finishByIndex(TEST_USER_ID, 674);
        Assert.assertNull(task);
    }

    @Test
    public void finishByIndexIncorrectUser() {
        @Nullable final Task task = taskRepository.finishByIndex(TEST_USER_ID_INCORRECT, 674);
        Assert.assertNull(task);
    }

    @Test
    public void changeStatusById() {
        @Nullable final Task task = taskRepository.changeStatusById(TEST_USER_ID, this.task.getId(), Status.NOT_STARTED);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
    }

    @Test
    public void changeStatusByName() {
        @Nullable final Task task = taskRepository.changeStatusByName(TEST_USER_ID, TEST_TASK_NAME, Status.NOT_STARTED);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
    }

    @Test
    public void changeStatusByIndex() {
        @Nullable final Task task = taskRepository.changeStatusByIndex(TEST_USER_ID, 0, Status.NOT_STARTED);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
    }

}