package ru.tsc.avramenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.exception.empty.EmptyIdException;
import ru.tsc.avramenko.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.avramenko.tm.exception.system.ProcessException;
import ru.tsc.avramenko.tm.model.Project;

import java.util.List;

public class ProjectRepositoryTest {

    @Nullable
    private ProjectRepository projectRepository;

    @Nullable
    private Project project;

    @NotNull
    protected static final String TEST_PROJECT_NAME = "TestName";

    @NotNull
    protected static final String TEST_DESCRIPTION_NAME = "TestDescription";

    @NotNull
    protected static final String TEST_USER_ID = "TestUserId";

    @NotNull
    protected static final String TEST_USER_ID_INCORRECT = "TestUserIdIncorrect";

    @NotNull
    protected static final String TEST_PROJECT_ID_INCORRECT = "647";

    @Before
    public void before() {
        projectRepository = new ProjectRepository();
        project = projectRepository.add(TEST_USER_ID, new Project(TEST_PROJECT_NAME, TEST_DESCRIPTION_NAME));
    }

    @Test
    public void add() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        Assert.assertNotNull(project.getName());
        Assert.assertNotNull(project.getDescription());
        Assert.assertEquals(TEST_PROJECT_NAME, project.getName());
        Assert.assertEquals(TEST_DESCRIPTION_NAME, project.getDescription());

        @NotNull final Project projectById = projectRepository.findById(project.getId());
        Assert.assertNotNull(projectById);
        Assert.assertEquals(project, projectById);
    }

    @Test
    public void findAll() {
        @NotNull final List<Project> projects = projectRepository.findAll();
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void existsById() {
        Assert.assertTrue(projectRepository.existsById(TEST_USER_ID, project.getId()));
    }

    @Test
    public void findAllByUserId() {
        @NotNull final List<Project> projects = projectRepository.findAll(TEST_USER_ID);
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void findAllByUserIdIncorrect() {
        @NotNull final List<Project> projects = projectRepository.findAll(TEST_USER_ID_INCORRECT);
        Assert.assertNotEquals(1, projects.size());
    }

    @Test
    public void findById() {
        @NotNull final Project project = projectRepository.findById(TEST_USER_ID, this.project.getId());
        Assert.assertNotNull(project);
    }

    @Test(expected = ProcessException.class)
    public void findByIdIncorrect() {
        @NotNull final Project project = projectRepository.findById(TEST_USER_ID, TEST_PROJECT_ID_INCORRECT);
        Assert.assertNull(project);
    }

    @Test(expected = ProcessException.class)
    public void findByIdNull() {
        @NotNull final Project project = projectRepository.findById(TEST_USER_ID, null);
        Assert.assertNull(project);
    }

    @Test(expected = ProcessException.class)
    public void findByIdIncorrectUser() {
        @NotNull final Project project = projectRepository.findById(TEST_USER_ID_INCORRECT, this.project.getId());
        Assert.assertNull(project);
    }

    @Test
    public void findByName() {
        @NotNull final Project project = projectRepository.findByName(TEST_USER_ID, TEST_PROJECT_NAME);
        Assert.assertNotNull(project);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void findByNameIncorrect() {
        @NotNull final Project project = projectRepository.findByName(TEST_USER_ID, TEST_PROJECT_ID_INCORRECT);
        Assert.assertNull(project);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void findByNameNull() {
        @NotNull final Project project = projectRepository.findByName(TEST_USER_ID, null);
        Assert.assertNull(project);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void findByNameIncorrectUser() {
        @NotNull final Project project = projectRepository.findByName(TEST_USER_ID_INCORRECT, this.project.getName());
        Assert.assertNull(project);
    }

    @Test
    public void findByIndex() {
        @NotNull final Project project = projectRepository.findByIndex(TEST_USER_ID, 0);
        Assert.assertNotNull(project);
    }
/*
    @Test(expected = ProcessException.class)
    public void removeByIdNull() {
        Assert.assertNull(projectRepository.removeById(TEST_USER_ID, null));
    }

    @Test(expected = ProcessException.class)
    public void removeByIdIncorrect() {
        @NotNull final Project project = projectRepository.removeById(TEST_USER_ID, TEST_PROJECT_ID_INCORRECT);
        Assert.assertNull(project);
    }

    @Test(expected = ProcessException.class)
    public void removeByIdIncorrectUser() {
        @NotNull final Project project = projectRepository.removeById(TEST_USER_ID_INCORRECT, this.project.getId());
        Assert.assertNull(project);
    }

    @Test
    public void removeByIndex() {
        @NotNull final Project project = projectRepository.removeByIndex(TEST_USER_ID, 0);
        Assert.assertNotNull(project);
    }

    @Test
    public void removeByName() {
        @NotNull final Project project = projectRepository.removeByName(TEST_USER_ID, TEST_PROJECT_NAME);
        Assert.assertNotNull(project);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void removeByNameIncorrect() {
        @NotNull final Project project = projectRepository.removeByName(TEST_USER_ID, TEST_PROJECT_ID_INCORRECT);
        Assert.assertNull(project);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void removeByNameNull() {
        @NotNull final Project project = projectRepository.removeByName(TEST_USER_ID, null);
        Assert.assertNull(project);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void removeByNameIncorrectUser() {
        @NotNull final Project project = projectRepository.removeByName(TEST_USER_ID_INCORRECT, this.project.getName());
        Assert.assertNull(project);
    }
*/
    @Test
    public void startById() {
        @Nullable final Project project = projectRepository.startById(TEST_USER_ID, this.project.getId());
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test(expected = ProcessException.class)
    public void startByIdNull() {
        @Nullable final Project project = projectRepository.startById(TEST_USER_ID, null);
        Assert.assertNull(project);
    }

    @Test(expected = ProcessException.class)
    public void startByIdIncorrect() {
        @Nullable final Project project = projectRepository.startById(TEST_USER_ID, TEST_PROJECT_ID_INCORRECT);
        Assert.assertNull(project);
    }

    @Test(expected = ProcessException.class)
    public void startByIdIncorrectUser() {
        @Nullable final Project project = projectRepository.startById(TEST_USER_ID_INCORRECT, this.project.getId());
        Assert.assertNull(project);
    }

    @Test
    public void startByName() {
        @Nullable final Project project = projectRepository.startByName(TEST_USER_ID, TEST_PROJECT_NAME);
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test(expected = ProjectNotFoundException.class)
    public void startByNameNull() {
        @Nullable final Project project = projectRepository.startByName(TEST_USER_ID, null);
        Assert.assertNull(project);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void startByNameIncorrect() {
        @Nullable final Project project = projectRepository.startByName(TEST_USER_ID, TEST_PROJECT_ID_INCORRECT);
        Assert.assertNull(project);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void startByNameIncorrectUser() {
        @Nullable final Project project = projectRepository.startByName(TEST_USER_ID_INCORRECT, TEST_PROJECT_NAME);
        Assert.assertNull(project);
    }

    @Test
    public void startByIndex() {
        @Nullable final Project project = projectRepository.startByIndex(TEST_USER_ID, 0);
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test
    public void startByIndexIncorrect() {
        @Nullable final Project project = projectRepository.startByIndex(TEST_USER_ID, 674);
        Assert.assertNull(project);
    }

    @Test
    public void startByIndexIncorrectUser() {
        @Nullable final Project project = projectRepository.startByIndex(TEST_USER_ID_INCORRECT, 674);
        Assert.assertNull(project);
    }

    @Test
    public void finishById() {
        @Nullable final Project project = projectRepository.finishById(TEST_USER_ID, this.project.getId());
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test(expected = ProcessException.class)
    public void finishByIdNull() {
        @Nullable final Project project = projectRepository.finishById(TEST_USER_ID, null);
        Assert.assertNull(project);
    }

    @Test(expected = ProcessException.class)
    public void finishByIdIncorrect() {
        @Nullable final Project project = projectRepository.finishById(TEST_USER_ID, TEST_PROJECT_ID_INCORRECT);
        Assert.assertNull(project);
    }

    @Test(expected = ProcessException.class)
    public void finishByIdIncorrectUser() {
        @Nullable final Project project = projectRepository.finishById(TEST_USER_ID_INCORRECT, this.project.getId());
        Assert.assertNull(project);
    }

    @Test
    public void finishByName() {
        @Nullable final Project project = projectRepository.finishByName(TEST_USER_ID, TEST_PROJECT_NAME);
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test(expected = ProjectNotFoundException.class)
    public void finishByNameNull() {
        @Nullable final Project project = projectRepository.finishByName(TEST_USER_ID, null);
        Assert.assertNull(project);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void finishByNameIncorrect() {
        @Nullable final Project project = projectRepository.finishByName(TEST_USER_ID, TEST_PROJECT_ID_INCORRECT);
        Assert.assertNull(project);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void finishByNameIncorrectUser() {
        @Nullable final Project project = projectRepository.finishByName(TEST_USER_ID_INCORRECT, TEST_PROJECT_NAME);
        Assert.assertNull(project);
    }

    @Test
    public void finishByIndex() {
        @Nullable final Project project = projectRepository.finishByIndex(TEST_USER_ID, 0);
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test
    public void finishByIndexIncorrect() {
        @Nullable final Project project = projectRepository.finishByIndex(TEST_USER_ID, 674);
        Assert.assertNull(project);
    }

    @Test
    public void finishByIndexIncorrectUser() {
        @Nullable final Project project = projectRepository.finishByIndex(TEST_USER_ID_INCORRECT, 674);
        Assert.assertNull(project);
    }

    @Test
    public void changeStatusById() {
        @Nullable final Project project = projectRepository.changeStatusById(TEST_USER_ID, this.project.getId(), Status.NOT_STARTED);
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
    }

    @Test
    public void changeStatusByName() {
        @Nullable final Project project = projectRepository.changeStatusByName(TEST_USER_ID, TEST_PROJECT_NAME, Status.NOT_STARTED);
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
    }

    @Test
    public void changeStatusByIndex() {
        @Nullable final Project project = projectRepository.changeStatusByIndex(TEST_USER_ID, 0, Status.NOT_STARTED);
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
    }

    @Test
    public void removeById() {
        projectRepository.removeById(TEST_USER_ID, project.getId());
        Assert.assertNull(projectRepository.findById(project.getId()));
    }

}