package ru.tsc.avramenko.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.avramenko.tm.component.Bootstrap;
import ru.tsc.avramenko.tm.marker.SoapCategory;

import java.util.List;

public class TaskEndpointTest {

    @NotNull
    protected static final Bootstrap bootstrap = new Bootstrap();

    @Nullable
    private static Session session;

    @Nullable
    private Task task;

    @Nullable
    private Project project;

    @BeforeClass
    public static void beforeClass() {
        session = bootstrap.getSessionEndpoint().openSession("User", "User");
        bootstrap.getTaskEndpoint().clearTask(session);
        bootstrap.getProjectEndpoint().clearProject(session);
    }

    @Before
    public void before() {
        bootstrap.getTaskEndpoint().createTask(session, "Task1", "TaskDesc1");
        bootstrap.getProjectEndpoint().createProject(session, "Project1", "ProjectDesc1");
        this.task = bootstrap.getTaskEndpoint().findTaskByName(session, "Task1");
        this.project = bootstrap.getProjectEndpoint().findProjectByName(session, "Project1");
    }

    @After
    public void after() {
        bootstrap.getTaskEndpoint().clearTask(session);
        bootstrap.getProjectEndpoint().clearProject(session);
    }

    @AfterClass
    public static void afterClass() {
        bootstrap.getSessionEndpoint().closeSession(session);
    }

    @Test
    @Category(SoapCategory.class)
    public void bindTaskById() {
        bootstrap.getTaskEndpoint().bindTaskById(session, project.getId(), task.getId());
        @NotNull final Task taskNew = bootstrap.getTaskEndpoint().findTaskById(session, task.getId());
        Assert.assertNotNull(taskNew.getProjectId());
    }

    @Test
    @Category(SoapCategory.class)
    public void findTaskAll() {
        @NotNull final List<Task> tasks = bootstrap.getTaskEndpoint().findTaskAll(session);
        Assert.assertNotNull(tasks);
    }

    @Test
    @Category(SoapCategory.class)
    public void findTaskByProjectId() {
        bootstrap.getTaskEndpoint().bindTaskById(session, project.getId(), task.getId());
        @NotNull final Task taskNew = bootstrap.getTaskEndpoint().findTaskById(session, task.getId());
        Assert.assertNotNull(taskNew.getProjectId());
        @NotNull final List<Task> list = bootstrap.getTaskEndpoint().findTaskByProjectId(session, taskNew.getProjectId());
        Assert.assertTrue(list.size()>0);
    }

    @Test
    @Category(SoapCategory.class)
    public void finishTaskById() {
        @NotNull final Task taskNew = bootstrap.getTaskEndpoint().finishTaskById(session, task.getId());
        Assert.assertNotNull(taskNew);
        Assert.assertEquals(Status.COMPLETED, taskNew.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void finishTaskByName() {
        @NotNull final Task taskNew = bootstrap.getTaskEndpoint().finishTaskByName(session, task.getName());
        Assert.assertNotNull(taskNew);
        Assert.assertEquals(Status.COMPLETED, taskNew.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void finishTaskByIndex() {
        @NotNull final Task task = bootstrap.getTaskEndpoint().finishTaskByIndex(session, 0);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void changeTaskStatusById() {
        @NotNull final Task taskNew = bootstrap.getTaskEndpoint().changeTaskStatusById(session, task.getId(), Status.NOT_STARTED);
        Assert.assertNotNull(taskNew);
        Assert.assertEquals(Status.NOT_STARTED, taskNew.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void changeTaskStatusByName() {
        @NotNull final Task taskNew = bootstrap.getTaskEndpoint().changeTaskStatusByName(session, task.getName(), Status.NOT_STARTED);
        Assert.assertNotNull(taskNew);
        Assert.assertEquals(Status.NOT_STARTED, taskNew.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void changeTaskStatusByIndex() {
        @NotNull final Task task = bootstrap.getTaskEndpoint().changeTaskStatusByIndex(session, 0, Status.NOT_STARTED);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void startTaskById() {
        @NotNull final Task taskNew = bootstrap.getTaskEndpoint().startTaskById(session, task.getId());
        Assert.assertNotNull(taskNew);
        Assert.assertEquals(Status.IN_PROGRESS, taskNew.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void startTaskByName() {
        @NotNull final Task taskNew = bootstrap.getTaskEndpoint().startTaskByName(session, task.getName());
        Assert.assertNotNull(taskNew);
        Assert.assertEquals(Status.IN_PROGRESS, taskNew.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void startTaskByIndex() {
        @NotNull final Task task = bootstrap.getTaskEndpoint().startTaskByIndex(session, 0);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void updateTaskById() {
        @NotNull final Task taskNew = bootstrap.getTaskEndpoint().updateTaskById(session, task.getId(), "NewTaskName", "NewTaskDesc");
        Assert.assertNotNull(taskNew);
        Assert.assertEquals("NewTaskName", taskNew.getName());
        Assert.assertEquals("NewTaskDesc", taskNew.getDescription());
    }

}